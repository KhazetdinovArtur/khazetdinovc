/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: archee
 *
 * Created on 18 февраля 2016 г., 13:54
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int func(int a) {
    if (a / 10 == 0) {
        cout << " " << a << " ";
        return a;
    } else {
        cout << " " << a % 10 << " ";
        func(a / 10);
    }
}

int main(int argc, char** argv) {
    int x;
    cout << "Введите число: ";
    cin>>x;
    func(x);

}


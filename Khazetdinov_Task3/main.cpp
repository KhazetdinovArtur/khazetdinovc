/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: archee
 *
 * Created on 25 февраля 2016 г., 14:59
 */

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

void mergeSort(vector<int> &a, size_t start, size_t end) {
    if (end - start < 2)
        return;
    if (end - start == 2) {
        if (a[start] > a[start + 1])
            swap(a[start], a[start + 1]);
        return;
    }
    mergeSort(a, start, start + (end - start) / 2);
    mergeSort(a, start + (end - start) / 2, end);
    vector<int> b;
    size_t b1 = start;
    size_t e1 = start + (end - start) / 2;
    size_t b2 = e1;
    while (b.size() < end - start) {
        if (b1 >= e1 || (b2 < end && a[b2] <= a[b1])) {
            b.push_back(a[b2]);
            ++b2;
        } else {
            b.push_back(a[b1]);
            ++b1;
        }
    }
    for (size_t i = start; i < end; ++i)
        a[i] = b[i - start];
}

int main() {
    vector<int> sort;
    for (int i = 0; i < 20; ++i)
        sort.push_back(i);
    for (size_t i = 0; i < sort.size(); ++i)
        swap(sort[i], sort[rand() % (sort.size() - i) + i]);
    for (int i = 0; i < 20; i++) {
        cout << sort[i] << " ";
    }
    cout << endl;
    mergeSort(sort, 0, sort.size());
    for (int i = 0; i < 20; i++) {
        cout << sort[i] << " ";
    }
    cout << endl;
}

